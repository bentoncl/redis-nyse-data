# Redis-NYSE Data

This dataset is copmrised of end of day data for 3,066 companies listed on the NYSE. Specifically, this dataset is end of day data for April 04, 2019. The dataset includes the company's ticker symbol, the day's openening, closing, highest, and lowest prices, the transaction volume for the day, and the date.

The data model is structured as follows:

key		value		
ticker	---->	name
		open
		high
		low
		close
		volume
		date

